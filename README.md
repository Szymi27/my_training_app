# my_training_app

A new Flutter project.

# Instalacja

1. flutter pub get

# Projekt zespołowy:

1. Szymon Kroplewski
2. Maciej Gajewski
3. Ignacy Michalik

# Opis

1. Pierwotnie aplikacja miała mieć 3 ekrany, ale po drodze były komplikacje i wyszły ostatecznie 2

2. Pierwszy ekran to przykładowe treningi dostępne w aplikacji, zdjęcie jest jedno, bo nie jest to finalny produkt aplikacji tylko wersja testowa

3. Drugi ekran to liczenie codziennych kalorii, produkt można dodać i zostanie wyświetlony, wstępnie miał być kod kreskowy, ale w połączeniu z bazą danych miał problemy, ze względu na wersję darmową firebase, wystąpiły ograniczenia co do niektórych funkcji

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
