import 'package:flutter/material.dart';

import '../../widgets/main_drawer.dart';
import '../exercises/training_screen.dart';
import '../calories_screen/daily_calories_screen.dart';

class TabsScreen extends StatefulWidget {
  const TabsScreen({super.key});

  @override
  State<TabsScreen> createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  int _selectedPageIndex = 0;

  final widgetTitle = [
    'Training',
    'Daily Calories',
  ];

  final widgetOptions = [
    const TrainingScreen(),
    const DailyCaloriesScreen(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widgetTitle.elementAt(_selectedPageIndex)),
      ),
      drawer: const Drawer(
        child: MainDrawer(),
      ),
      body: Center(
        child: widgetOptions.elementAt(_selectedPageIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.star),
            label: 'Training',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.star),
            label: 'Daily Calories',
          ),
        ],
        selectedIconTheme: const IconThemeData(opacity: 0.0, size: 0.0),
        unselectedIconTheme: const IconThemeData(opacity: 0.0, size: 0.0),
        unselectedFontSize: 16,
        selectedFontSize: 18,
        backgroundColor: Theme.of(context).colorScheme.primary,
        unselectedItemColor: Colors.white,
        selectedItemColor: Theme.of(context).colorScheme.secondary,
        currentIndex: _selectedPageIndex,
        onTap: _onItemTapped,
      ),
    );
  }
}
