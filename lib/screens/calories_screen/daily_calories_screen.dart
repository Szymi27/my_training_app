import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../widgets/calories/add_food_button.dart';
import '../../model/food_track_task.dart';
import '../../widgets/calories/calorie_stats.dart';
import '../../services/database.dart';
import '../../widgets/calories/food_track_list.dart';

class DailyCaloriesScreen extends StatefulWidget {
  const DailyCaloriesScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _DailyCaloriesScreen();
  }
}

class _DailyCaloriesScreen extends State<DailyCaloriesScreen> {
  double servingSize = 0;
  String dropdownValue = 'grams';
  DateTime _value = DateTime.now();
  DateTime today = DateTime.now();
  Color _rightArrowColor = const Color(0xffC1C1C1);
  final Color _leftArrowColor = const Color(0xffC1C1C1);

  DatabaseService databaseService = DatabaseService(
      uid: 'flutter-training-ad201', currentDate: DateTime.now());

  Widget _calorieCounter() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border(
                bottom: BorderSide(
              color: Colors.grey.withOpacity(0.5),
              width: 1.5,
            ))),
        height: 220,
        child: Row(
          children: <Widget>[
            CalorieStats(datePicked: _value),
          ],
        ),
      ),
    );
  }

  Future _selectDate() async {
    DateTime? picked = await showDatePicker(
      context: context,
      initialDate: _value,
      firstDate: DateTime(2019),
      lastDate: DateTime.now(),
      builder: (BuildContext context, Widget? child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: const Color(0xff5FA55A),
          ),
          child: child!,
        );
      },
    );
    if (picked != null) setState(() => _value = picked);
    _stateSetter();
  }

  void _stateSetter() {
    if (today.difference(_value).compareTo(const Duration(days: 1)) == -1) {
      setState(() => _rightArrowColor = const Color(0xffEDEDED));
    } else {
      setState(() => _rightArrowColor = Colors.white);
    }
  }

  Widget _showDatePicker() {
    return SizedBox(
      width: 250,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          IconButton(
            key: const Key("left_arrow_button"),
            icon: const Icon(Icons.arrow_left, size: 25.0),
            color: _leftArrowColor,
            onPressed: () {
              setState(() {
                _value = _value.subtract(const Duration(days: 1));
                _rightArrowColor = Colors.white;
              });
            },
          ),
          TextButton(
            onPressed: () => _selectDate(),
            child: Text(_dateFormatter(_value),
                style: const TextStyle(
                  fontFamily: 'Open Sans',
                  fontSize: 18.0,
                  fontWeight: FontWeight.w700,
                  color: Colors.black,
                )),
          ),
          IconButton(
              key: const Key("right_arrow_button"),
              icon: const Icon(Icons.arrow_right, size: 25.0),
              color: _rightArrowColor,
              onPressed: () {
                if (today
                        .difference(_value)
                        .compareTo(const Duration(days: 1)) ==
                    -1) {
                  setState(() {
                    _rightArrowColor = const Color(0xffC1C1C1);
                  });
                } else {
                  setState(() {
                    _value = _value.add(const Duration(days: 1));
                  });
                  if (today
                          .difference(_value)
                          .compareTo(const Duration(days: 1)) ==
                      -1) {
                    setState(() {
                      _rightArrowColor = const Color(0xffC1C1C1);
                    });
                  }
                }
              }),
        ],
      ),
    );
  }

  String _dateFormatter(DateTime tm) {
    DateTime today = DateTime.now();
    Duration oneDay = const Duration(days: 1);
    Duration twoDay = const Duration(days: 2);
    String month;

    switch (tm.month) {
      case 1:
        month = "Jan";
        break;
      case 2:
        month = "Feb";
        break;
      case 3:
        month = "Mar";
        break;
      case 4:
        month = "Apr";
        break;
      case 5:
        month = "May";
        break;
      case 6:
        month = "Jun";
        break;
      case 7:
        month = "Jul";
        break;
      case 8:
        month = "Aug";
        break;
      case 9:
        month = "Sep";
        break;
      case 10:
        month = "Oct";
        break;
      case 11:
        month = "Nov";
        break;
      case 12:
        month = "Dec";
        break;
      default:
        month = "Undefined";
        break;
    }

    Duration difference = today.difference(tm);

    if (difference.compareTo(oneDay) < 1) {
      return "Dzisiaj";
    } else if (difference.compareTo(twoDay) < 1) {
      return "Wczoraj";
    } else {
      return "${tm.day} $month ${tm.year}";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      appBar: AppBar(
          backgroundColor: Colors.grey,
          elevation: 0,
          bottom: PreferredSize(
            preferredSize: const Size.fromHeight(5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                _showDatePicker(),
                AddFoodButton(databaseService: databaseService),
              ],
            ),
          )),
      body: StreamProvider<List<FoodTrackTask>>.value(
        initialData: const [],
        value: DatabaseService(
                uid: "flutter-training-ad201", currentDate: DateTime.now())
            .foodTracks,
        child: Column(children: <Widget>[
          _calorieCounter(),
          Expanded(
              key: const Key("food_track_list"),
              child: ListView(
                children: <Widget>[FoodTrackList(datePicked: _value)],
              ))
        ]),
      ),
    );
  }
}
