import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

import '../../widgets/training/video_player.dart';

class ExerciseScreen extends StatefulWidget {
  final String nameId;

  const ExerciseScreen(this.nameId, {super.key});

  @override
  State<ExerciseScreen> createState() => _ExerciseScreenState();
}

class _ExerciseScreenState extends State<ExerciseScreen> {
  final pageController = PageController();
  var number = 0;
  String currentExercise = '';

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: FirebaseFirestore.instance
          .collection('exercises')
          .doc(widget.nameId)
          .collection('exercises')
          .snapshots(),
      builder: (
        BuildContext context,
        AsyncSnapshot<QuerySnapshot> snapshot,
      ) {
        if (snapshot.hasData) {
          final snap = snapshot.data!.docs;
          return Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              title: Text(
                  currentExercise != '' ? currentExercise : snap.first['name']),
              centerTitle: true,
              elevation: 0,
            ),
            extendBodyBehindAppBar: true,
            body: PageView.builder(
              itemCount: snap.length,
              controller: pageController,
              onPageChanged: (index) => setState(() {
                currentExercise = snap[index]['name'];
                number = snap[index]['noofreps'];
              }),
              itemBuilder: (BuildContext context, int index) {
                return VideoPlayerWidget(
                  videoUrl: snap[index]['videoUrl'],
                  onInitialized: () => setState(() {}),
                  pageController: pageController,
                  number: number != 0 ? number : snap.first['noofreps'],
                );
              },
            ),
          );
        } else {
          return const Text('wrong');
        }
      },
    );
  }
}
