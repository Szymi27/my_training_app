import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../model/food_track_task.dart';
import '../../services/database.dart';
import 'food_track_tile.dart';

class FoodTrackList extends StatelessWidget {
  final DateTime datePicked;
  late List<dynamic> curFoodTracks;
  late DatabaseService databaseService;
  FoodTrackList({super.key, required this.datePicked});

  @override
  Widget build(BuildContext context) {
    final DateTime curDate =
        DateTime(datePicked.year, datePicked.month, datePicked.day);

    final foodTracks = Provider.of<List<FoodTrackTask>>(context);

    List findCurFoodTracks(List foodTrackFeed) {
      List curFoodTracks = [];
      for (var foodTrack in foodTrackFeed) {
        DateTime createdDate = DateTime(foodTrack.createdOn.year,
            foodTrack.createdOn.month, foodTrack.createdOn.day);
        if (createdDate.compareTo(curDate) == 0) {
          curFoodTracks.add(foodTrack);
        }
      }
      return curFoodTracks;
    }

    curFoodTracks = findCurFoodTracks(foodTracks);

    return ListView.builder(
      scrollDirection: Axis.vertical,
      physics: const ClampingScrollPhysics(),
      shrinkWrap: true,
      itemCount: curFoodTracks.length + 1,
      itemBuilder: (context, index) {
        if (index < curFoodTracks.length) {
          return FoodTrackTile(
              foodTrackEntry: curFoodTracks[index], keyValue: index);
        } else {
          return const SizedBox(height: 5);
        }
      },
    );
  }

  Future<void> loadFromMockDatabase() async {
    databaseService = DatabaseService(
        uid: "flutter-training-ad201", currentDate: DateTime.now());
    curFoodTracks.add(await databaseService.loadFoodTrackEntryToDatabase());
  }
}
