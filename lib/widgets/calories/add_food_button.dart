import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../model/food_track_task.dart';
import '../../services/database.dart';

class AddFoodButton extends StatefulWidget {
  final DatabaseService databaseService;
  const AddFoodButton({Key? key, required this.databaseService})
      : super(key: key);

  @override
  State<AddFoodButton> createState() => _AddFoodButtonState();
}

class _AddFoodButtonState extends State<AddFoodButton> {
  DateTime value = DateTime.now();
  late FoodTrackTask addFoodTrack;
  final _addFoodKey = GlobalKey<FormState>();
  double servingSize = 0;

  @override
  void initState() {
    super.initState();
    addFoodTrack = FoodTrackTask(
        food_name: "",
        calories: 0,
        carbs: 0,
        protein: 0,
        fat: 0,
        mealTime: "",
        createdOn: value,
        grams: 0);
  }

  void resetFoodTrack() {
    addFoodTrack = FoodTrackTask(
        food_name: "",
        calories: 0,
        carbs: 0,
        protein: 0,
        fat: 0,
        mealTime: "",
        createdOn: value,
        grams: 0);
  }

  @override
  Widget build(BuildContext context) {
    return IconButton(
      key: const Key('add_food_modal_button'),
      icon: const Icon(Icons.add_box),
      iconSize: 25,
      color: Colors.white,
      onPressed: () async {
        setState(() {});
        _showFoodToAdd(context);
      },
    );
  }

  _showFoodToAdd(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Dodaj posiłek"),
            content: _showAmountHad(),
            key: const Key("add_food_modal"),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.pop(context), // passing false
                child: const Text('Cancel'),
              ),
              TextButton(
                onPressed: () async {
                  if (checkFormValid()) {
                    Navigator.pop(context);
                    var random = Random();
                    int randomMilliSecond = random.nextInt(1000);
                    addFoodTrack.createdOn = value;
                    addFoodTrack.createdOn = addFoodTrack.createdOn
                        .add(Duration(milliseconds: randomMilliSecond));
                    widget.databaseService.addFoodTrackEntry(addFoodTrack);
                    resetFoodTrack();
                  } else {
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      content: Text("Wszystkie pola muszą być większe niż 0"),
                      backgroundColor: Colors.white,
                    ));
                  }
                },
                child: const Text('Ok', key: Key("add_food_modal_submit")),
              ),
            ],
          );
        });
  }

  checkFormValid() {
    if (addFoodTrack.calories != 0 &&
        addFoodTrack.carbs != 0 &&
        addFoodTrack.protein != 0 &&
        addFoodTrack.fat != 0 &&
        addFoodTrack.grams != 0) {
      return true;
    }
    return false;
  }

  Widget _showAmountHad() {
    return Scaffold(
      body: Column(children: <Widget>[
        _showAddFoodForm(),
        _showUserAmount(),
      ]),
    );
  }

  Widget _showAddFoodForm() {
    return Form(
      key: _addFoodKey,
      child: Column(children: [
        TextFormField(
          key: const Key('add_food_modal_food_name_field'),
          decoration: const InputDecoration(
              labelText: "Nazwa *", hintText: "Wprowadź nazwę posiłku"),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "Wprowadź nazwę posiłku";
            }
            return null;
          },
          onChanged: (value) {
            addFoodTrack.food_name = value;
          },
        ),
        TextFormField(
          key: const Key('add_food_modal_calorie_field'),
          decoration: const InputDecoration(
              labelText: "Kalorie *", hintText: "Wprowadź ilość kalorii"),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "Wprowadź ilość kalorii";
            }
            return null;
          },
          keyboardType: TextInputType.number,
          onChanged: (value) {
            try {
              addFoodTrack.calories = int.parse(value);
            } catch (e) {
              addFoodTrack.calories = 0;
            }

            // addFood.calories = value;
          },
        ),
        TextFormField(
          key: const Key('add_food_modal_carbs_field'),
          decoration: const InputDecoration(
              labelText: "Węglowodany *",
              hintText: "Wprowadź ilość węglowodanów"),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "Wprowadź ilość węglowodanów";
            }
            return null;
          },
          keyboardType: TextInputType.number,
          onChanged: (value) {
            try {
              addFoodTrack.carbs = int.parse(value);
            } catch (e) {
              addFoodTrack.carbs = 0;
            }
          },
        ),
        TextFormField(
          key: const Key('add_food_modal_protein_field'),
          decoration: const InputDecoration(
              labelText: "Białko *", hintText: "Wprowadź ilość białka"),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "Wprowadź ilość białka";
            }
            return null;
          },
          onChanged: (value) {
            try {
              addFoodTrack.protein = int.parse(value);
            } catch (e) {
              addFoodTrack.protein = 0;
            }
          },
        ),
        TextFormField(
          key: const Key('add_food_modal_fat_field'),
          decoration: const InputDecoration(
              labelText: "Tłuszcze *", hintText: "Wprowadź ilość tłuszczów"),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "Wprowadź ilość tłuszczów";
            }
            return null;
          },
          onChanged: (value) {
            try {
              addFoodTrack.fat = int.parse(value);
            } catch (e) {
              addFoodTrack.fat = 0;
            }
          },
        ),
      ]),
    );
  }

  Widget _showUserAmount() {
    return Expanded(
      child: TextField(
          key: const Key("add_food_modal_grams_field"),
          maxLines: 1,
          autofocus: true,
          decoration: const InputDecoration(
              labelText: 'Gramy *',
              hintText: 'np. 100',
              contentPadding: EdgeInsets.all(0.0)),
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
          ],
          onChanged: (value) {
            try {
              addFoodTrack.grams = int.parse(value);
            } catch (e) {
              addFoodTrack.grams = 0;
            }
            setState(() {
              servingSize = double.tryParse(value) ?? 0;
            });
          }),
    );
  }
}
