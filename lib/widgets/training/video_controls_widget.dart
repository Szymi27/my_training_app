import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class VideoControlsWidget extends StatelessWidget {
  final VideoPlayerController controller;
  final VoidCallback onRewindVideo;
  final VoidCallback onNextVideo;
  final ValueChanged<bool> onTogglePlaying;
  final int number;

  const VideoControlsWidget(
      {Key? key,
      required this.controller,
      required this.onRewindVideo,
      required this.onNextVideo,
      required this.onTogglePlaying,
      required this.number})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: Colors.white.withOpacity(0.95),
      ),
      height: 142,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              buildText(title: 'Duration', value: '${45} Seconds'),
              buildText(title: 'Reps', value: '$number Reps'),
            ],
          ),
          buildButtons(context)
        ],
      ),
    );
  }

  Widget buildText({
    required String title,
    required String value,
  }) =>
      Column(
        children: [
          Text(
            title,
            style: const TextStyle(
              color: Colors.grey,
            ),
          ),
          SizedBox(
              height: 16,
              child: Text(
                value,
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ))
        ],
      );

  Widget buildButtons(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          IconButton(
            onPressed: onRewindVideo,
            icon: const Icon(
              Icons.fast_rewind,
              color: Colors.black87,
            ),
          ),
          buildPlayButton(context),
          IconButton(
            onPressed: onNextVideo,
            icon: const Icon(
              Icons.fast_forward,
              color: Colors.black87,
            ),
          )
        ],
      );

  Widget buildPlayButton(BuildContext context) {
    if (controller.value.isPlaying) {
      return buildButton(
        context: context,
        icon: const Icon(Icons.pause, size: 30, color: Colors.white),
        onClicked: () => onTogglePlaying(false),
      );
    } else {
      return buildButton(
        context: context,
        icon: const Icon(Icons.play_arrow, size: 30, color: Colors.white),
        onClicked: () => onTogglePlaying(true),
      );
    }
  }

  Widget buildButton({
    required BuildContext context,
    required Widget icon,
    required VoidCallback onClicked,
  }) =>
      GestureDetector(
        onTap: onClicked,
        child: Container(
          decoration: const BoxDecoration(shape: BoxShape.circle, boxShadow: [
            BoxShadow(
              color: Color(0xFFff6369),
              blurRadius: 8,
              offset: Offset(2, 2),
            ),
          ]),
          child: CircleAvatar(
            radius: 24,
            child: icon,
          ),
        ),
      );
}
