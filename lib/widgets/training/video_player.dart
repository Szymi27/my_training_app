import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

import 'video_controls_widget.dart';

class VideoPlayerWidget extends StatefulWidget {
  final String videoUrl;
  final VoidCallback onInitialized;
  final PageController pageController;
  final int number;

  const VideoPlayerWidget(
      {super.key,
      required this.videoUrl,
      required this.onInitialized,
      required this.pageController,
      required this.number});

  @override
  State<VideoPlayerWidget> createState() => _VideoPlayerWidgetState();
}

class _VideoPlayerWidgetState extends State<VideoPlayerWidget> {
  late VideoPlayerController controller;

  @override
  void initState() {
    super.initState();
    controller = VideoPlayerController.network(widget.videoUrl)
      ..initialize().then((value) => {
            controller.setLooping(true),
            controller.play(),
            widget.onInitialized(),
          });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SizedBox(
          height: displayHeight(context),
          width: displayWidth(context),
          child: VideoPlayer(controller),
        ),
        Positioned(
          bottom: 20,
          right: 50,
          left: 50,
          child: buildVideoControls(),
        ),
      ],
    );
  }

  double displayHeight(BuildContext context) {
    return MediaQuery.of(context).size.height;
  }

  double displayWidth(BuildContext context) {
    return MediaQuery.of(context).size.width;
  }

  buildVideoControls() => VideoControlsWidget(
        controller: controller,
        onRewindVideo: () => widget.pageController.previousPage(
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeIn,
        ),
        onNextVideo: () => widget.pageController.nextPage(
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeIn,
        ),
        onTogglePlaying: (isPlaying) {
          setState(() {
            if (isPlaying) {
              controller.play();
            } else {
              controller.pause();
            }
          });
        },
        number: widget.number,
      );
}
