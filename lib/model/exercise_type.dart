import 'package:flutter/material.dart';

enum ExerciseType { beginner, intermediate, advanced }

String getExerciseName(ExerciseType type) {
  switch (type) {
    case ExerciseType.advanced:
      return 'Advanced';
      break;
    case ExerciseType.intermediate:
      return 'Intermediate';
      break;
    case ExerciseType.beginner:
      return 'Beginner';
      break;
    default:
      return 'All';
      break;
  }
}
